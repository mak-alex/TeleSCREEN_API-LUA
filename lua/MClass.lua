local makclass = {
  _VERSION = 'MClass v0.0.1',
  _DESCRIPTION = 'Object Orientation for Lua',
  _URL = 'https://bitbucket.org/enlab/MClass',
  _LICENSE = [[
    MIT LICENSE
    Copyright (c) 2015 Mikhailenko Alexandr Konstantinovich (a.k.a) Alex M.A.K
    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ]]
}
local debug = require('debug')
local debug_getinfo,getmetatable,pairs,
      rawget,rawset,require,setmetatable,
      tostring,type,MAKClass = debug.getinfo,
      getmetatable,pairs,rawget,rawset,
      require,setmetatable,tostring,type,{}

local function megaFunction(self, ...)
  local frame = debug_getinfo(2)
  local mt = getmetatable(self)
  assert(mt and mt.makBase, 'There are no super method')
  local func = mt.makBase[frame.name]
  return func and func(self, ...) or nil
end

function MAKClass.class(name, struct, parent)
  if type(name) ~= 'string' then name, struct, parent = nil, name, struct end
  if type(parent) == 'string' then parent = require(parent) end
  local cls = {}
  name = name or ('Cls' .. tostring(cls):sub(10))
  struct = struct or {}
  -- создание новой инстанции класса без вызова конструкторов
  local function _create_inst()
    local base = parent and parent:create() or nil
    local inst = {}
    setmetatable(inst, { __base = base,
      __index = setmetatable({ super = megaFunction, }, {
      __index = function(_, key)
      local idx_func = rawget(inst, '__index__')
      if idx_func then return idx_func(inst, key) end end, 
      }),
    })
    if base then for k, v in pairs(base) do inst[k] = v end end
    for k, v in pairs(struct) do inst[k] = v end
    inst.__class = cls
    return inst
  end

  setmetatable(cls, { __index = setmetatable({ 
    __name = name, name = MAKClass.name, subclass = MAKClass.subclass, create = _create_inst, },{
    __index = function(_, key) if parent then return parent[key] end end,}),
    __newindex = function(tbl, key, val) if key ~= 'name' then rawset(tbl, key, val) end end,
    __call = function(cls, ...)
    local inst = cls:create()
    local new = inst.new
    if new then new(inst, ...) end
      return inst
    end,
  })
  return cls
end

function MAKClass.subclass(parent, name)
  return function(struct) return name and MAKClass.class(name, struct, parent) or MAKClass.class(struct, parent) end
end

function MAKClass.name(name)
  return {
    class = function(struct, parent) return MAKClass.class(name, struct) end,
    subclass = function(parent) return function(struct) return MAKClass.class(name, struct, parent) end end,
  }
end

return MAKClass
