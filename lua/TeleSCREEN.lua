package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";

local MCASTMUX_HOME                   = os.getenv("MCASTMUX_HOME");
local lscript_path                    = MCASTMUX_HOME.."/lua"
local JSONf                           = assert(loadfile(lscript_path.."/JSON.lua"));
local JSON                            = JSONf()
JSON.strictTypes                      = true
local zapi                            = require("zapi");
                                        require("LuaXML")
local XML                             = xml
local soap                            = require("CMDBuildSOAPAPI")
local rus                             = require("alphabet")
local m                               = require("MClass")
local socket                          = require("socket")
local ltn12                           = require("ltn12")
local http                            = require("socket.http")
local url                             = require("socket.url")
                                        require("logging")
                                        require("logging.file")

local logger                          = logging.file("telescreen%s.log","%Y-%m-%d")
local TeleSCREEN = {
  ["AboutME"]={
      _MODULE         = "TeleSCREEN",
      _VERSION        = "TeleSCREEN v0.1.0-rc1",
      _AUTHOR         = "Alexandr Mikhailenko (he also) FlashHacker",
      _URL            = "https://bitbucket.org/enlab/TeleSCREEN",
      _MAIL           = "flashhacker1988@gmail.com",
      _COPYRIGHT      = "Design Bureau of Industrial Communications LTD, 2015. All rights reserved.",
      _LICENSE        = [[
        MIT LICENSE
        Copyright (c) 2015 Mikhailenko Alexandr Konstantinovich (a.k.a) Alex pseudoclass.A.K
        Permission is hereby granted, free of charge, to any person obtaining a
        copy of this software and associated documentation files (the
        "Software"), to deal in the Software without restriction, including
        without limitation the rights to use, copy, modify, merge, publish,
        distribute, sublicense, and/or sell copies of the Software, and to
        permit persons to whom the Software is furnished to do so, subject to
        the following conditions:
        The above copyright notice and this permission notice shall be included
        in all copies or substantial portions of the Software.
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
        OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
        IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
        CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
        TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
        SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
        ]],
      _DESCRIPTION    = [[
        Dependencies:
            logging # luarocks install logging --local
        How to use:
          package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";
          local MCASTMUX_HOME           = os.getenv("MCASTMUX_HOME");
          local lscript_path            = MCASTMUX_HOME.."/lua"
          local JSONf                   = assert(loadfile(lscript_path.."/JSON.lua"));
          local JSON                    = JSONf()
          JSON.strictTypes              = true
          local WGET                    = require("wget")
          local ts                      = require("TeleSCREEN")

          local TeleSCREEN_URL          = "http://10.244.244.79:9125/TeleSCREEN/config/getServices"
          local programNum              = {
                                            [1]=65534,[2]=102,[3]=112,[4]=104,[5]=105,[6]=107,[7]=110,[8]=111,
                                            [9]=193,[10]=211,[11]=6546,[12]=2268,[13]=2269,[14]=2277,[15]=1,
                                          }

          function main()
            WGET.get(TeleSCREEN_URL, "status.json")                                  -- cached in TeleSCREEN JSON file
            JsonArray=JSON:decode(fileToString(MCASTMUX_HOME,'status.json'))         -- open... ;-)
            if arg[1]=="true" or arg[2]=="true" then debug=true else debug=false end
            if type(tonumber(arg[1]))=="number" then rexton=tonumber(arg[1]) end
            if type(tonumber(arg[2]))=="number" then rexton=tonumber(arg[2]) end
            
            if rexton==nil then
              for k,v in pairs(programNum) do ts.parse("igmp://239.1.1.3:2000",v,nil,debug) end
            else
              ts.parse("igmp://239.1.1.3:2000",rexton,nil,debug)
            end
          end

          main(arg[1],arg[2])
        How to use in terminal:
          lua test.lua true             # Print all elementss "Debug"
          lua test.lua 65534 true       # Print filter element "Debug"
          lua test.lua                  # Print all elements

        Need add:
          function for get errors
      ]]
  }, ["isin"]=function(str,array)
    local theResult=false
    for i,v in ipairs(array) do if(v == str) then theResult=true end end return theResult
  end, ["xml_escape"]=function(value)
    return value:gsub("&","&amp;"):gsub([["]],"&quot;"):gsub("'","&apos;"):gsub("<","&lt;");
  end, ["correct_name"]=function(value)
    return value:gsub("/","_"):gsub(":","_"):gsub("#","-"):gsub(";","_");
  end, ["isempty"]=function(s)
    return s == nil or s == '' or s == 'table'
  end, ["formatBitrate"]=function(num, n)
    if num==nil then return 0 end
    local mult = 10^(n or 0)
    return math.floor(num * mult + 0.5) / mult
  end,
  ["getError"]=function(url)
    local arrayError,dataError,getNextTime={},{},0
    for k,v in pairs(ErrorArray[1]) do
      if k=="next" then print("next_alarm: "..v) end
      if k=="alarms" then 
        table.foreach(v,function(k,v)
          if v.url==url then
            table.foreach(v,function(kk,vv)
              arrayError[kk]=vv
            end)
          end
        end) 
      end
    end
    return {
      arrayError, 
      getNextTime,
      table.foreach(arrayError,print)
    }
  end
}

local function formatBitrateScaled(br)
  if (br<1000) then return tostring(br.." bits")
    elseif (br<100000) then return tostring((br/1000).." kbits")
    elseif (br<1000000) then return tostring(TeleSCREEN.formatBitrate(br/1000).." kbits")
    elseif (br<100000000) then return tostring((br/1000000).." mbits")
  end
  return tostring(TeleSCREEN.formatBitrate(br/1000000).." mbits")
end

-- recursive Print (structure, limit, indent)
local function rPrint(s, l, i)                                       
  l = (l) or 100000000; i = i or "";                                -- default item limit, indent string
  if (l<1) then print "ERROR: Item limit reached."; logger:error("ERROR: Item limit reached.") return l-1 end;
  local ts = type(s);
  if (ts ~= "table") then print (i,ts,s); if type(s)~="boolean" then logger:info(i.." "..s) end return l-1 end
  print (i,ts); --logger:info(i.." "..ts)                                 -- print "table"
  for k,v in pairs(s) do l = rPrint(v, l, i.."\t["..tostring(k).."]"); if (l < 0) then break end end
  return l
end

function httpPost(base_url,query)
  if not (base_url and query)
  then
      error("Get(): base_url and query are mandatory arguments")
      return nil
  end
  local response_body={}
  zapi.socket.http.request{
    url=base_url,
    method="POST",
    headers = {
--      ["Accept-Encoding"] = "gzip, deflate",
      ["Content-Type"] = "application/json",
      ["Content-Length"] = string.len(query)
    },
    source = zapi.ltn12.source.string(query),
    sink = zapi.ltn12.sink.table(response_body)
  }
  return response_body;
end

function sysExec(args)
  fd=io.popen(args)
  result = fd:read('*all')
  return result
end

function fileToString(location, filename)
    if filename ~= nil then
      local path = (location or MCASTMUX_HOME) .. "/" .. filename
      local jsonConfigFile = assert(io.open(path, "r"))
      local stringFile = jsonConfigFile:read "*a"
      jsonConfigFile:close()
      print( "Converting JSON cache file "..filename.." to LUA Array - done")
      logger:info("Converting JSON cache file  "..filename.." to LUA Array - done")
      return stringFile
    else
      error( "variable location and file name are empty or not correctly filled in")
      logger:error("variable location and file name are empty or not correctly filled in")
      return 1
    end
  end

function stringToFile(location, filename, data)
    if location and filename and data ~= nil then
      local path = (location or MCASTMUX_HOME) .. "/" .. filename -- определяем путь и имя файла
      file = assert(io.open(path, "w"))                   -- открываем файл в режиме записи
      io.output(file)                                     -- указываем режим вход или выход
      io.write(data, "\n")                                -- записываем наши данные в файл
      io.close(file)                                      -- закрываем файл чтобы не висел в памяти
      print( "create file "..filename.." - done")
      logger:info("create file "..filename.." - done")
      return 0
    else
      error("variable location and filename/data are empty or not correctly filled in")
      logger:error("variable location and filename/data are empty or not correctly filled in")
      return 1
    end
end

return {
  ["ident"]=function(ZAPI_URL,ZAPI_LOGIN,ZAPI_PASS)
    if zapi then
      zapi.login(ZAPI_URL,ZAPI_LOGIN,ZAPI_PASS);
      -- zapi.tracetoggle()
    else
      error("zapi does not exists");
    end
  end,
  ["zapiGetKey"]=function()
    print('test')
  end,
  -- Parse bitrate from TeleSCREEN channel
  -- @argument rf - rf
  -- @argument program - 1 .. 65534
  -- @argument sid - 1 .. 65534 -- sort
  -- @argument debug - true or false
  ["parse"]=function(rf,program,sid,debug)
    local resultBit,okon,flags={},{},{}
    if type(JsonArray)~="table" then error("incorrect type "..JsonArray) logger:error("incorrect type "..JsonArray) end
    logger:info("SID: "..program) print("\nSID: \t"..program)
    for i=1, #JsonArray[1].ts[rf].programs do
      --print(JsonArray[1].ts[rf].programs[i].url)
      TeleSCREEN.getError(JsonArray[1].ts[rf].programs[i].url)
      --table.foreach(JsonArray[1].ts[rf].programs[i].url,print)
        for k,v in pairs(JsonArray[1].ts[rf].programs[i].psi) do
          if k=="program" and v==(program or 1) then
            for ii=1, #JsonArray[1].ts[rf].programs[i].psi.pids do
              for k_pids,v_pids in pairs(JsonArray[1].ts[rf].programs[i].psi.pids[ii]) do
                if k_pids=="pid" then
                  for k_btr,v_btr in pairs(JsonArray[1].ts[rf].pids) do
                    if sid==nil then
                      if tonumber(k_btr)==tonumber(v_pids) then 
                        logger:info("PID: \t"..k_btr) print("PID: \t"..k_btr)
                        for kk_bit,vv_bit in pairs(v_btr) do
                          if kk_bit=="bitrate" then table.insert(resultBit,vv_bit) end
                        end
                        if debug==true then rPrint(v_btr) end
                      end
                    else
                      if tonumber(k_btr)==tonumber(sid) then
                        if not flags[sid] then
                          logger:info("PID: \t"..sid) print("PID: \t"..sid)
                          for kk_bit,vv_bit in pairs(v_btr) do
                            if kk_bit=="bitrate" then resultBit=vv_bit end
                          end
                          flags[sid]=true
                        end
                        if debug==true then rPrint(v_btr) end
                      end                  
                    end
                  end
                end
              end
            end
          end
        end
      end
      if sid==nil then
        local count = #resultBit
        if type(resultBit)=="table" and count==1 then
          print("FBITRATE: \t"..formatBitrateScaled(TeleSCREEN.formatBitrate(tonumber(resultBit[1]))), 
                "\nBITRATE : \t"..tonumber(resultBit[1]).."\n\n\t\t###\n"
              )
          logger:info("BITRATE: "..formatBitrateScaled(TeleSCREEN.formatBitrate(tonumber(resultBit[1]))).."\n")
          return formatBitrateScaled(TeleSCREEN.formatBitrate(tonumber(resultBit[1])))
        elseif type(resultBit)=="table" and count==2 then
          local result=tonumber(resultBit[1])+tonumber(resultBit[2])
          print("FBITRATE: \t"..formatBitrateScaled(TeleSCREEN.formatBitrate(result)), 
                "\nBITRATE : \t"..result.."\n\n\t\t###\n"
              )
          logger:info("BITRATE: "..formatBitrateScaled(TeleSCREEN.formatBitrate(tonumber(result))).."\n")
        elseif type(resultBit)=="table" and count==3 then
          local result=tonumber(resultBit[1])+tonumber(resultBit[2])+tonumber(resultBit[3])
          print("FBITRATE: \t"..formatBitrateScaled(TeleSCREEN.formatBitrate(tonumber(result))), 
                "\nBITRATE : \t"..tonumber(result).."\n\n\t\t###\n"
              )
          logger:info("BITRATE: "..formatBitrateScaled(TeleSCREEN.formatBitrate(tonumber(result))).."\n")

        end
      end
  end
}
