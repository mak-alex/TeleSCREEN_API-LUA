package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";
local MCASTMUX_HOME 				  = os.getenv("MCASTMUX_HOME");
local lscript_path 					  = MCASTMUX_HOME.."/lua"
local JSONf 						      = assert(loadfile(lscript_path.."/JSON.lua"));
local JSON 							      = JSONf()
JSON.strictTypes 					    = true
local WGET                    = require("wget")
local ts                      = require("TeleSCREEN")

local TeleSCREEN_URL          = "http://10.244.244.79:9125/TeleSCREEN/config/getServices"
local programNum              = {
                                  [1]=65534,[2]=102,[3]=112,[4]=104,[5]=105,[6]=107,[7]=110,[8]=111,
                                  [9]=193,[10]=211,[11]=6546,[12]=2268,[13]=2269,[14]=2277,[15]=1,
                                }

function main()
  WGET.get(TeleSCREEN_URL, "status.json")                                  -- cached in TeleSCREEN JSON file
  ErrorArray=JSON:decode(fileToString(MCASTMUX_HOME,'error.json'))
  JsonArray=JSON:decode(fileToString(MCASTMUX_HOME,'status.json'))         -- open... ;-)
  if arg[1]=="true" or arg[2]=="true" then debug=true else debug=false end
  if type(tonumber(arg[1]))=="number" then rexton=tonumber(arg[1]) end
  if type(tonumber(arg[2]))=="number" then rexton=tonumber(arg[2]) end
  
  if rexton==nil then
	  for k,v in pairs(programNum) do ts.parse("igmp://239.1.1.3:2000",v,nil,debug) end
  else
  	ts.status("igmp://239.1.1.3:2000",rexton,nil,debug)
  end
end
--local posixtime=sysExec("date +%s.%3N")
--local rex=httpPost("http://10.244.244.79:9125/TeleSCREEN/config/status",'[{"next_alarm":"'..posixtime..'","streams":true}]')
--table.foreach(rex,print)
main(arg[1],arg[2])